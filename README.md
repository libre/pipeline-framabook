Ce dépôt fournit les outils de base pour déployer un pipeline de production d’epubs conformes aux recommandations d’accessibilité en ne se basant que sur des outils en ligne de commande dans un terminal shell Bash. Il contient également un fichier source en markdown qui permet de générer le manuel d’usage de ce pipeline.

Pour l’installer, il suffit de se placer dans le répertoire de son choix puis de lancer la commande :

    git clone git@framagit.org:YannKervran/pipeline-framabook.git

Comme certains dossiers du pipeline sont initialement vides à l’installation du pipeline, il faut veiller à les avoir à la racine de celui-ci. Il faut donc vérifier que le répertoire créé par la commande précédente (« pipeline-framabook » par défaut) contienne les répertoires suivants (au besoin, les créer) :

- build
- deflate
- src
- tmp
- work

Pour pouvoir lancer les commandes de base, il est nécessaire d’avoir installé sur son système :

- pandoc
- zip / unzip
- make
- git

Les autres logiciels nécessaires pour la vérification, accessoires sont :

- epubcheck
- ace (via nodejs, voir [la page d’installation du Consortium Daisy](https://daisy.github.io/ace/getting-started/installation/))

Il suffit ensuite de taper depuis la racine du dépôt la commande :

    make draft

pour obtenir dans le sous-répertoire 'build' le manuel d’utilisation sous format epub. La source en est bien évidemment dans le sous-répertoire 'src'.


