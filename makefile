firstpass:
	@ rm -Rf deflate/mimetype deflate/META-INF/ deflate/EPUB/ deflate/OEBPS/
	@ pandoc src/*.md -o tmp/draft1.epub
	@ unzip tmp/draft1.epub -d deflate
	@ rm tmp/draft1.epub

dezip:
	@ rm -Rf deflate/mimetype deflate/META-INF/ deflate/EPUB/ deflate/OEBPS/
	@ unzip src/*.epub -d deflate

epub:
	@ make -C deflate

draft: firstpass epub

validate: build/monepub_final.epub
	@ - epubcheck -j build/epucheck_report.json build/monepub_final.epub # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ ace --force --outdir build/ace_reports build/monepub_final.epub

clean:
	@ rm -Rf deflate/mimetype deflate/META-INF/ deflate/EPUB/
	@ rm -Rf build/*
	@ rm -Rf tmp/*


